default-backend
===============

#### links
- https://hub.docker.com/repository/docker/labdens/default-backend
- https://github.com/nuvo/default-backend

```bash
# build docker image
. build.sh
docker push labdens/default-backend

# kube
vagrant up
ANSIBLE_HOST_KEY_CHECKING=false ansible-playbook playbook.yml -i inventory -l rke

wget https://github.com/rancher/rke/releases/download/v1.0.0/rke_linux-amd64
chmod +x rke_linux-amd64
./rke_linux-amd64 up --config cluster.yml

export KUBECONFIG=$(pwd)/kube_config_cluster.yml
kubectl get pods --all-namespaces

# helm
kubectl apply -f create-tiller-rbac-sa.yml
helm init --service-account tiller
kubectl -n kube-system get po


# no ingress, cluster_no_ingress.yml
helm install stable/nginx-ingress --name nginx-ingress -f nginx.yml --wait
helm install stable/nginx-ingress --name nginx-ingress --set controller.service.type=ClusterIP --set controller.hostNetwork=true

# generate crt for nginx
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout key -out crt
```
